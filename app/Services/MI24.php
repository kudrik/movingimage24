<?php

namespace App\Services;

use GuzzleHttp\Client;


class MI24
{
    private $url = 'https://api-qa.video-cdn.net/v1/vms';

    private $token;

    private $videoManagerId;

    public function __construct()
    {
        $this->generateToken();
    }

    private function query($path, $method = 'GET', $parameters = array() )
    {
        $url = $this->url . $path;

        //set header
        $headers = array();

        if ($this->token) {
            $headers['Authorization'] = 'Bearer '.$this->token;
        }

        //http client
        $client = new Client(['headers'=>$headers]);

        $response = false;

        if ($method == 'POST') {

            $response = $client->post($url, [
                'json' => $parameters
            ]);

        } else {

            //if GET
            $response = $client->get($url, [
                'form_params' => $parameters,
            ]);
        }

        if ($response) {
            return json_decode($response->getBody(), true);
        }

        return false;
    }

    private function generateToken()
    {
        $this->token = '';

        $fields = [
            'username' => config('mi24.username'),
            'password' => config('mi24.password'),
        ];

        $result = $this->query('/auth/login', 'POST', $fields);

        if (isset($result['accessToken'])) {
            $this->token = $result['accessToken'];
        }

        if (isset($result['validForVideoManager'])) {
            $this->videoManagerId = $result['validForVideoManager'];
        }
    }

    public function getVideoUrlList($videoid)
    {
        return $this->query('/'.$this->videoManagerId.'/videos/'.$videoid.'/download-urls');
    }

    public function getVideoUrlByHeight($videoid, $height) {

        if ($list = $this->getVideoUrlList($videoid)) {

            $max_q = 0;
            $best_i = 0;

            //try to find best quality and mp4 is priority
            foreach ($list as $k=>$row) {

                if (!isset($row['url'])) {
                    continue;
                }

                $quality = intval($row['quality']);

                if (($quality > $max_q) || ($quality == $max_q && $row['fileExtension'] == 'mp4')) {
                    $best_i = $k;
                    $max_q = $quality;
                }
            }

            if (isset($list[$best_i]) && isset($list[$best_i]['url'])) {

                return $list[$best_i]['url'];
            }
        }

        return false;
    }
}