<?php

namespace App\Services;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Image;
use App\Exceptions\ApiException;
use App\Services\MI24;

class Screenshot
{
    public function get($videoid, $time = 0, $width = null, $height = null)
    {
        $time = intval($time);

        if (!($time>0)) {
            $time = 5;
        }

        $mi24 = new MI24();

        if (! ($video_url = $mi24->getVideoUrlByHeight($videoid, $height))) {
            throw new ApiException('It is impossible to create a screenshot');
        }

        $video_file_tmp = sys_get_temp_dir().'/'.md5($videoid.'_'.microtime());

        $image_file_tmp = sys_get_temp_dir().'/'.md5($videoid.'_'.$time.'_'.$width.'_'.$height.'_'.microtime());

        //copy video to temp folder
        if (!copy($video_url, $video_file_tmp )) {
            throw new ApiException('It is impossible to create a screenshot');
        }

        //make screenshots
        $process = new Process(config('ffmpeg.path').' -y -ss ' . gmdate("H:i:s", $time) . ' -i ' . $video_file_tmp . ' -vframes 1 -f image2 ' . $image_file_tmp . ' 2>&1');
        $process->run();

        unlink($video_file_tmp);

        if (is_file($image_file_tmp)) {

            //proccess image
            try {

                $image = Image::make($image_file_tmp);

                if ($width>0 || $height>0) {

                    if ($width>2000) {
                        $width = 2000;
                    }

                    if ($height>2000) {
                        $height = 2000;
                    }

                    $image->resize($width, $height,  function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                $w = $image->width();
                $h = $image->height();

                $image_name = '/images/'.$videoid.'_'.$time.'_'.$w.'_'.$h.'.jpg';

                $image->save(public_path().$image_name, 100);

                unlink($image_file_tmp);

                return array(
                    'videoid'=>$videoid,
                    'offset'=>$time,
                    'image'=>url($image_name),
                    'width'=>$w,
                    'height' =>$h
                );

            } catch (\Exception $e) {
                unlink($image_file_tmp);
            }
        }

        throw new ApiException('It is impossible to create a screenshot');
    }
}