<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\String_;
use App\Http\Controllers\ApiController;
use App\Services\Screenshot;

class ScreenshotController extends ApiController
{
    public function index(Request $request, string $videoid)
    {
        $screenshot = new Screenshot();

        if ($imageInfo = $screenshot->get($videoid, $request->get('time'), $request->get('w'), $request->get('h'))) {

            return $this->response_json($imageInfo);
        }
    }
}