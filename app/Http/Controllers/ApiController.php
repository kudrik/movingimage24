<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\String_;


class ApiController extends Controller
{
    public function response_json($obj)
    {
        if (is_array($obj)) {
            $obj['status'] = 200;
        }
        return response()->json($obj);
    }
}