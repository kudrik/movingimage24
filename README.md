# Moving Images 24 Screenshots API #

* Version 1.0.0


### Installation ###

* clone the repository into folder
* run composer install
```
#!bash

composer install
```

* folders public/images, storage/*, bootstrap/cache should be writable.

* setup your webserver to public dir as web_root dir.
 or run laravel

```
#!bash
 
php artisan serve
```

### Configuration ###

there is folder config/
you can change mi24 login/password api in config/mi24.php

### Example api request ###
[your domain]/api/screenshots/2vkGzLuB-mSHJKgDSPbsUF?time=5&w=800&h=600

### Requirements ###
* php v7.1.0
* laravel requirements https://laravel.com/docs/5.4/installation
* image.intervention req http://image.intervention.io/getting_started/installation